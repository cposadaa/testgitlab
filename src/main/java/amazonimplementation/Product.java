package amazonimplementation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Product {

    private String productName;
    private int price;
    private static final Logger LOGGER = LogManager.getLogger(Product.class);
    public Product(String productName, int price) {
        this.productName = productName;
        this.price = price;
        LOGGER.info("Poduct "+ productName);
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }



}
