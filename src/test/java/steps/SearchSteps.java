package steps;

import amazonimplementation.Product;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class SearchSteps {

    private static final Logger LOGGER = LogManager.getLogger(SearchSteps.class);
    Product product;

    @Before
    public void setUp() {
        try(
                InputStream input = new FileInputStream("src/main/resources/tests.properties")
        ) {
            Properties prop = new Properties();
            prop.load(input);


            Path resourceDirectory = Paths.get("src", "main", "resources", "file3.log");

            File logFile = new File(resourceDirectory.toString());
            logFile.createNewFile();
            FileOutputStream s = new FileOutputStream(logFile, false);

            System.out.println("Absolute Path" + logFile.getAbsolutePath());
            System.setProperty("logFilename", logFile.getAbsolutePath());


        } catch (IOException ex) {
            ex.printStackTrace();
            Assertions.fail("Could not load the config file");
        }

    }

    @Given("I have a search field on Amazon Page")
    public void iHaveASearchFieldOnAmazonPage() {


        System.out.println("Step 1");
        LOGGER.info("Step 1aSDFsdfa q235 szd ");
        LOGGER.info("log level info");
        LOGGER.warn("Log level warn");
        LOGGER.error("Lg level error");
    }

    @When("I search for a product with name {string} and price {int}")
    public void iSearchForAProductWithNameAndPrice(String productName, int price) {
        System.out.println("Name: " + productName + " Price: " + price);
        product = new Product(productName, price);
    }

    @Then("Product with name {string} Should be displayed")
    public void productWithNameShouldBeDisplayed(String productName) {
        System.out.println("Product Name: " + productName);
        Assertions.assertEquals(productName, productName);
    }
}
